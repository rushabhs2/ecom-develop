# from msilib.schema import Condition
# from statistics import mode
# from tkinter import CASCADE
# from turtle import color
# from unicodedata import category, name
# from django.db import models
# from pytz import timezone
from datetime import date, datetime
import email
from email import message
from email.headerregistry import Address
from operator import mod
from sre_parse import State
from statistics import mode
from types import CoroutineType
from ckeditor.fields import RichTextField
from distutils.command.upload import upload
from itertools import product
from pyexpat import model
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.

class Categories(models.Model):
    name=models.CharField(max_length=200)

    def __str__(self) :
        return self.name

class Brand(models.Model):
    name=models.CharField(max_length=200)

    def __str__(self) :
        return self.name

class Color(models.Model):
    name=models.CharField(max_length=200)
    code=models.CharField(max_length=50)

    def __str__(self) :
        return self.name

class FILTER_PRICE(models.Model):
    FILTER_PRICE=(
        ('100 TO 1000','100 TO 1000'),
        ('1000 TO 10000','1000 TO 10000'),
        ('10000 TO 20000','10000 TO 20000'),
        ('20000 TO 30000','20000 TO 30000'),
        ('30000 TO 40000','30000 TO 40000'),
        ('40000 TO 50000','40000 TO 50000'),
    )
    price=models.CharField(choices=FILTER_PRICE,max_length=60)

    def __str__(self) :
        return self.price

class Product(models.Model):
    CONDITION =(('New','New'),('Old','Old'))
    STOCK=(('IN STOCK','IN STOCK'),('OUT OF STOCK','OUT OF STOCK'))
    STATUS =(('Publish','Publish'),('Draft','Draft'))

    unique_id=models.CharField(unique=True,max_length=200,null=True,blank=True)
    image=models.ImageField(upload_to='Product_images/img')
    name=models.CharField(max_length=200)
    price=models.IntegerField()
    condition=models.CharField(choices=CONDITION,max_length=100)
    information=RichTextField(null=True)
    description=RichTextField(null=True)
    stock=models.CharField(choices=STOCK,max_length=200)
    status=models.CharField(choices=STATUS,max_length=200)
    created_date=models.DateTimeField(default=timezone.now)

    Categories=models.ForeignKey(Categories,on_delete=models.CASCADE)
    brand=models.ForeignKey(Brand,on_delete=models.CASCADE)
    color=models.ForeignKey(Color,on_delete=models.CASCADE)
    filter_price=models.ForeignKey(FILTER_PRICE,on_delete=models.CASCADE)
    

    def save(self,*args,**kwargs):
        if self.unique_id is None and self.created_date and self.id:
            self.unique_id=self.created_date.strftime('75%Y%m%d25')+str(self.id)
        return super().save(*args,**kwargs)

    def __str__(self) :
        return self.name


class Images(models.Model):
    image=models.ImageField(upload_to='Product_images/img')
    product=models.ForeignKey(Product,on_delete=models.CASCADE)


class Tag(models.Model):
    name=models.CharField(max_length=200)
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    def __str__(self) :
        return self.name

class contact_us(models.Model):
    name=models.CharField(max_length=200)
    email=models.EmailField(max_length=200)
    subject=models.CharField(max_length=200)
    message=models.TextField()

    def __str__(self):
            return self.name


class Order(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    firstname=models.CharField(max_length=100)
    lastname=models.CharField(max_length=100)
    county=models.CharField(max_length=100)
    address=models.TextField()
    city=models.CharField(max_length=100)
    state=models.CharField(max_length=100)
    postcode=models.IntegerField()
    phone=models.IntegerField()
    email=models.EmailField(max_length=100)
    additional_info=models.TextField()
    amount=models.CharField(max_length=100)
    payment_id=models.CharField(max_length=300,null=True,blank=True)
    paid=models.BooleanField(default=False,null=True)
    date=models.DateField(default=datetime.today)

    def __str__(self) :
        return self.user.username

class orderiteam(models.Model):
    order=models.ForeignKey(Order,on_delete=models.CASCADE)
    product=models.CharField(max_length=200)
    image=models.ImageField(upload_to='Product_images/orrder_img')
    quantity=models.CharField(max_length=20)
    price=models.CharField(max_length=50)
    total=models.CharField(max_length=1000)

    def __str__(self):
        return self.order.user.username
