
from pydoc import classname
import weakref
from django.contrib import admin

# Register your models here.
from . models import *




# Tublerinline start

admin.site.register(Images)
admin.site.register(Tag)

class ImagesTublerinline(admin.TabularInline):
    model=Images

class TagTublerinline(admin.TabularInline):
    model=Tag

class ProductAdmin(admin.ModelAdmin):
    inlines=[ImagesTublerinline,TagTublerinline ]


class orderiteamTubleinline(admin.TabularInline):
    model=orderiteam

class OrderAdmin(admin.ModelAdmin):
    inlines=[orderiteamTubleinline]
    list_display=['firstname','phone','email','payment_id','paid','date']
    search_fields=['firstname','email','payment_id']


# TuberInline End 

admin.site.register(Categories)
admin.site.register(Brand)
admin.site.register(Color)
admin.site.register(FILTER_PRICE)
admin.site.register(Product,ProductAdmin)
admin.site.register(contact_us)
admin.site.register(Order,OrderAdmin)
admin.site.register(orderiteam)
